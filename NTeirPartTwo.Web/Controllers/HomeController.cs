﻿using System.Web.Mvc;
using NTeirPartTwo.Core.Infastructure.DependencyManagement;

namespace NTeirPartTwo.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Msae = "Your contact page.";
            
            return View();
        }
    }
}
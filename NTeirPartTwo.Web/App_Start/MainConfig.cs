﻿using System;
using NTeirPartTwo.Core.Infastructure;

namespace NTeirPartTwo.Web
{
    public class MainConfig
    {
        public static void RegisterSettings()
        {
            EngineContext.Initialize(false);
        }
    }
}